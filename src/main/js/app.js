const React = require('react');
const ReactDOM = require('react-dom');
const client = require('./client');
const prueba = require('./test');


class App extends React.Component {

	constructor(props) {
		super(props);
		this.state = {documentos: []};
	}

	componentDidMount() {
		client({method: 'GET', path: '/connector/documents'}).done(response => {
			this.setState({documentos: response.entity});
		});
	}

	render() {
		return (
			<DocumentosList documentos={this.state.documentos}/>
		)
	}
}

class DocumentosList extends React.Component {
	render(){
		const documentos = this.props.documentos.map(documento => 
			<Documento key={documento.x_documento} documento={documento}/>
					);
		return (
				<table> 
						<tbody>
								<tr>
										<th>Id</th>
										<th>Nivel</th>
										<th>Denominación</th>
										<th>Fecha inicial</th>
										<th>Fecha final</th>
										<th>Signatura</th>
										<th>Signat. Compl.</th>
										<th>Estado custodia</th>
										<th>Remisor</th>
										<th>Productor</th>
										<th>Serie documental</th>
								</tr>
								{documentos}
						</tbody>
				</table>
			)
	}
}

class Documento extends React.Component{
	render(){
		return (
				<tr>
						<td>{this.props.documento.x_documento}</td>
						<td>{this.props.documento.d_tipoNodo}</td>
						<td>{this.props.documento.d_titulo}</td>
						<td>{this.props.documento.c_anoinicial}</td>
						<td>{this.props.documento.c_anofinal}</td>
						<td>{this.props.documento.c_signatura}</td>
						<td>{this.props.documento.d_signaturacomplementaria}</td>
						<td>{this.props.documento.edo_x_estadodocumento}</td>
						<td>{this.props.documento.d_remisor_i}</td>
						<td>{this.props.documento.pro_x_productor}</td>
						<td>{this.props.documento.d_cuadro}</td>
				</tr>
				)
	}
}
ReactDOM.render(
		<App />, 
		document.getElementById('react')
		)