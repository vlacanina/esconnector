package org.ayesa.test.ESConnector.model;

import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Mapping;
import org.springframework.data.elasticsearch.annotations.Setting;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

@Document(indexName = "#{@indexDocument}", type = "#{@indexType}")
@Setting(settingPath = "/elasticsearch/settings/settings-documento.json")
@Mapping(mappingPath = "/elasticsearch/mapping/mapping-documento.json")
public class DocumentoDAO {

	private Long archivo;
	private String tipo;
	@Id
	private Long xDocumento;
	private String remisor;
	private String remisor_i;
	private String claveExp;
	private String tituloExp;
	private String claveTramitacion;
	private String tituloTramitacion;
	private String nig;
	private String naturaleza;
	private Date prescadu;
	private Date terparejecu;
	private Date resprescadu;
	private String objeto;
	private String estadoJudicial;
	private String procedimientoRel;
	private String observaciones;
	private Long estadoDocumento_h;
	private String referencia;
	private String descripcion_i;
	private String volumen;
	private String histArchivo;
	private String ingreso_i;
	private String nuevosIng;
	private String iDescripcion;
	private String originales;
	private String copias;
	private String udescrela;
	private String nArchivero;
	private String normas;
	private String organizacion;
	private String reproduccion;
	private String lengua;
	private Long tipoNodo_c;
	private String parteRemitida;
	private Long estadoDocumento;
	private Long docXDocumento;
	private Long tipoNodo_d;
	private String tercerosEIntervinientes;
	private Long cuadroRemisor;
	private Long cuadroSerie;
	private Long ingreso;
	private String productor_i;
	private Long cuadroProductor;
	private Long ingresoAno;
	private Long codigoIngreso;
	private Long tipoIngreso;
	private Long estadoIngreso;
	private Long tipoNodo_i;
	private Date fechaExtremaInicial;
	private Date fechaExtremaFinal;
	private Long isadg;
	private String productor_is;
	private String alcance;
	private String histins;
	private String valoracion;
	private String acceso;
	private String cFisicas;
	private String nPublicaciones;
	private String notas;
	private Long diaInicial;
	private Long mesInicial;
	private Long anoInicial;
	private Long diaFinal;
	private Long mesFinal;
	private Long anoFInal;
	private String titulo;
	private Long cuadro;
	private String dCuadro;
	private Long cuaXCuadro;
	private Long isaXIsadgC;
	private Long rseXSerie;
	private Long signatura;
	private Long cSignatura;
	private String etiqueta;
	private String tipoNodo;
	private String lSerie;
	private String lPublicado;
	private String lEnviadoWeb;
	private Long tnoXTipoNodo_atn;
	private String fDocumento;
	private String referenciaSerie;
	private String signaturaComplementaria;
	private Long cSalida;
	private Long cSalidaAnio;
	private Long proXProductor;

	@JsonCreator
	public DocumentoDAO(@JsonProperty("arc_x_archivo") Long archivo, @JsonProperty("c_tipo") String tipo,
			@JsonProperty("x_documento") Long xDocumento, @JsonProperty("d_remisor") String remisor,
			@JsonProperty("d_remisor_i") String remisor_i, @JsonProperty("d_claveexp") String claveExp,
			@JsonProperty("d_tituloexp") String tituloExp, @JsonProperty("d_clavetramitacion") String claveTramitacion,
			@JsonProperty("d_titulotramitacion") String tituloTramitacion, @JsonProperty("d_nig") String nig,
			@JsonProperty("d_naturaleza") String naturaleza, @JsonProperty("f_prescadu") Date prescadu,
			@JsonProperty("f_terparejecu") Date terparejecu, @JsonProperty("f_resprescadu") Date resprescadu,
			@JsonProperty("d_objeto") String objeto, @JsonProperty("d_estadojudicial") String estadoJudicial,
			@JsonProperty("t_procedimientosrelacionados") String procedimientoRel,
			@JsonProperty("t_observaciones") String observaciones,
			@JsonProperty("edo_x_estadodocumento_h") Long estadoDocumento_h,
			@JsonProperty("c_referencia") String referencia, @JsonProperty("d_descripcion_i") String descripcion_i,
			@JsonProperty("t_volumen") String volumen, @JsonProperty("t_histarchiv") String histArchivo,
			@JsonProperty("c_ingreso_i") String ingreso_i, @JsonProperty("t_nuevosing") String nuevosIng,
			@JsonProperty("d_idescripcion") String iDescripcion, @JsonProperty("d_loriginales") String originales,
			@JsonProperty("d_lcopias") String copias, @JsonProperty("d_udescrela") String udescrela,
			@JsonProperty("t_narchivero") String nArchivero, @JsonProperty("t_normas") String normas,
			@JsonProperty("t_organizacion") String organizacion, @JsonProperty("d_reproduccion") String reproduccion,
			@JsonProperty("d_lengua") String lengua, @JsonProperty("tno_x_tiponodo_c") Long tipoNodo_c,
			@JsonProperty("d_parteremitida") String parteRemitida,
			@JsonProperty("edo_x_estadodocumento") Long estadoDocumento,
			@JsonProperty("doc_x_documento") Long docXDocumento, @JsonProperty("tno_x_tiponodo_d") Long tipoNodo_d,
			@JsonProperty("d_terceroseintervinientes") String tercerosEIntervinientes,
			@JsonProperty("cua_x_cuadroremisor") Long cuadroRemisor,
			@JsonProperty("cua_x_cuadroserie") Long cuadroSerie, @JsonProperty("x_ingreso") Long ingreso,
			@JsonProperty("d_productor_i") String productor_i, @JsonProperty("cua_x_productor") Long cuadroProductor,
			@JsonProperty("c_ingresoano") Long ingresoAno, @JsonProperty("c_ingreso") Long codigoIngreso,
			@JsonProperty("tin_x_tiposingreso") Long tipoIngreso,
			@JsonProperty("ein_x_estadosingreso") Long estadoIngreso, @JsonProperty("tno_x_tiponodo_i") Long tipoNodo_i,
			@JsonProperty("f_fecha_extrema_inicial") Date fechaExtremaInicial,
			@JsonProperty("f_fecha_extrema_final") Date fechaExtremaFinal, @JsonProperty("x_isadg") Long isadg,
			@JsonProperty("d_productor_is") String productor_is, @JsonProperty("t_alcance") String alcance,
			@JsonProperty("t_histins") String histins, @JsonProperty("t_valoracion") String valoracion,
			@JsonProperty("d_acceso") String acceso, @JsonProperty("d_cfisicas") String cFisicas,
			@JsonProperty("t_npublicaciones") String nPublicaciones, @JsonProperty("t_notas") String notas,
			@JsonProperty("c_diainicial") Long diaInicial, @JsonProperty("c_mesinicial") Long mesInicial,
			@JsonProperty("c_anoinicial") Long anoInicial, @JsonProperty("c_diafinal") Long diaFinal,
			@JsonProperty("c_mesfinal") Long mesFinal, @JsonProperty("c_anofinal") Long anoFInal,
			@JsonProperty("d_titulo") String titulo, @JsonProperty("x_cuadro") Long cuadro,
			@JsonProperty("d_cuadro") String dCuadro, @JsonProperty("cua_x_cuadro") Long cuaXCuadro,
			@JsonProperty("isa_x_isadg_c") Long isaXIsadgC, @JsonProperty("rse_x_serie") Long rseXSerie,
			@JsonProperty("x_signatura") Long signatura, @JsonProperty("c_signatura") Long cSignatura,
			@JsonProperty("d_etiqueta") String etiqueta, @JsonProperty("d_tiponodo") String tipoNodo,
			@JsonProperty("l_serie") String lSerie, @JsonProperty("l_publicado") String lPublicado,
			@JsonProperty("l_enviadoweb") String lEnviadoWeb,
			@JsonProperty("tno_x_tiponodo_atn") Long tnoXTipoNodo_atn, @JsonProperty("f_documento") String fDocumento,
			@JsonProperty("c_referenciaserie") String referenciaSerie,
			@JsonProperty("d_signaturacomplementaria") String signaturaComplementaria,
			@JsonProperty("c_salida") Long cSalida, @JsonProperty("c_salidaano") Long cSalidaAnio,
			@JsonProperty("pro_x_productor") Long proXProductor) {
		this.archivo = archivo;
		this.tipo = tipo;
		this.xDocumento = xDocumento;
		this.remisor = remisor;
		this.remisor_i = remisor_i;
		this.claveExp = claveExp;
		this.tituloExp = tituloExp;
		this.claveTramitacion = claveTramitacion;
		this.tituloTramitacion = tituloTramitacion;
		this.nig = nig;
		this.naturaleza = naturaleza;
		this.prescadu = prescadu;
		this.terparejecu = terparejecu;
		this.resprescadu = resprescadu;
		this.objeto = objeto;
		this.estadoJudicial = estadoJudicial;
		this.procedimientoRel = procedimientoRel;
		this.observaciones = observaciones;
		this.estadoDocumento_h = estadoDocumento_h;
		this.referencia = referencia;
		this.descripcion_i = descripcion_i;
		this.volumen = volumen;
		this.histArchivo = histArchivo;
		this.ingreso_i = ingreso_i;
		this.nuevosIng = nuevosIng;
		this.iDescripcion = iDescripcion;
		this.originales = originales;
		this.copias = copias;
		this.udescrela = udescrela;
		this.nArchivero = nArchivero;
		this.normas = normas;
		this.organizacion = organizacion;
		this.reproduccion = reproduccion;
		this.lengua = lengua;
		this.tipoNodo_c = tipoNodo_c;
		this.parteRemitida = parteRemitida;
		this.estadoDocumento = estadoDocumento;
		this.docXDocumento = docXDocumento;
		this.tipoNodo_d = tipoNodo_d;
		this.tercerosEIntervinientes = tercerosEIntervinientes;
		this.cuadroRemisor = cuadroRemisor;
		this.cuadroSerie = cuadroSerie;
		this.ingreso = ingreso;
		this.productor_i = productor_i;
		this.cuadroProductor = cuadroProductor;
		this.ingresoAno = ingresoAno;
		this.codigoIngreso = codigoIngreso;
		this.tipoIngreso = tipoIngreso;
		this.estadoIngreso = estadoIngreso;
		this.tipoNodo_i = tipoNodo_i;
		this.fechaExtremaInicial = fechaExtremaInicial;
		this.fechaExtremaFinal = fechaExtremaFinal;
		this.isadg = isadg;
		this.productor_is = productor_is;
		this.alcance = alcance;
		this.histins = histins;
		this.valoracion = valoracion;
		this.acceso = acceso;
		this.cFisicas = cFisicas;
		this.nPublicaciones = nPublicaciones;
		this.notas = notas;
		this.diaInicial = diaInicial;
		this.mesInicial = mesInicial;
		this.anoInicial = anoInicial;
		this.diaFinal = diaFinal;
		this.mesFinal = mesFinal;
		this.anoFInal = anoFInal;
		this.titulo = titulo;
		this.cuadro = cuadro;
		this.dCuadro = dCuadro;
		this.cuaXCuadro = cuaXCuadro;
		this.isaXIsadgC = isaXIsadgC;
		this.rseXSerie = rseXSerie;
		this.signatura = signatura;
		this.cSignatura = cSignatura;
		this.etiqueta = etiqueta;
		this.tipoNodo = tipoNodo;
		this.lSerie = lSerie;
		this.lPublicado = lPublicado;
		this.lEnviadoWeb = lEnviadoWeb;
		this.tnoXTipoNodo_atn = tnoXTipoNodo_atn;
		this.fDocumento = fDocumento;
		this.referenciaSerie = referenciaSerie;
		this.signaturaComplementaria = signaturaComplementaria;
		this.cSalida = cSalida;
		this.cSalidaAnio = cSalidaAnio;
		this.proXProductor = proXProductor;
	}

	@JsonProperty("arc_x_archivo")
	public Long getArchivo() {
		return archivo;
	}

	@JsonProperty("c_tipo")
	public String getTipo() {
		return tipo;
	}

	@JsonProperty("x_documento")
	public Long getxDocumento() {
		return xDocumento;
	}

	@JsonProperty("d_remisor")
	public String getRemisor() {
		return remisor;
	}

	@JsonProperty("d_remisor_i")
	public String getRemisor_i() {
		return remisor_i;
	}

	@JsonProperty("d_claveexp")
	public String getClaveExp() {
		return claveExp;
	}

	@JsonProperty("d_tituloexp")
	public String getTituloExp() {
		return tituloExp;
	}

	@JsonProperty("d_clavetramitacion")
	public String getClaveTramitacion() {
		return claveTramitacion;
	}

	@JsonProperty("d_titulotramitacion")
	public String getTituloTramitacion() {
		return tituloTramitacion;
	}

	@JsonProperty("d_nig")
	public String getNig() {
		return nig;
	}

	@JsonProperty("d_naturaleza")
	public String getNaturaleza() {
		return naturaleza;
	}

	@JsonProperty("f_prescadu")
	public Date getPrescadu() {
		return prescadu;
	}

	@JsonProperty("f_terparejecu")
	public Date getTerparejecu() {
		return terparejecu;
	}

	@JsonProperty("f_resprescadu")
	public Date getResprescadu() {
		return resprescadu;
	}

	@JsonProperty("d_objeto")
	public String getObjeto() {
		return objeto;
	}

	@JsonProperty("d_estadojudicial")
	public String getEstadoJudicial() {
		return estadoJudicial;
	}

	@JsonProperty("t_procedimientosrelacionados")
	public String getProcedimientoRel() {
		return procedimientoRel;
	}

	@JsonProperty("t_observaciones")
	public String getObservaciones() {
		return observaciones;
	}

	@JsonProperty("edo_x_estadodocumento_h")
	public Long getEstadoDocumento_h() {
		return estadoDocumento_h;
	}

	@JsonProperty("c_referencia")
	public String getReferencia() {
		return referencia;
	}

	@JsonProperty("d_descripcion_i")
	public String getDescripcion_i() {
		return descripcion_i;
	}

	@JsonProperty("t_volumen")
	public String getVolumen() {
		return volumen;
	}

	@JsonProperty("t_histarchiv")
	public String getHistArchivo() {
		return histArchivo;
	}

	@JsonProperty("c_ingreso_i")
	public String getIngreso_i() {
		return ingreso_i;
	}

	@JsonProperty("t_nuevosing")
	public String getNuevosIng() {
		return nuevosIng;
	}

	@JsonProperty("d_idescripcion")
	public String getiDescripcion() {
		return iDescripcion;
	}

	@JsonProperty("d_loriginales")
	public String getOriginales() {
		return originales;
	}

	@JsonProperty("d_lcopias")
	public String getCopias() {
		return copias;
	}

	@JsonProperty("d_udescrela")
	public String getUdescrela() {
		return udescrela;
	}

	@JsonProperty("t_narchivero")
	public String getnArchivero() {
		return nArchivero;
	}

	@JsonProperty("t_normas")
	public String getNormas() {
		return normas;
	}

	@JsonProperty("t_organizacion")
	public String getOrganizacion() {
		return organizacion;
	}

	@JsonProperty("d_reproduccion")
	public String getReproduccion() {
		return reproduccion;
	}

	@JsonProperty("d_lengua")
	public String getLengua() {
		return lengua;
	}

	@JsonProperty("tno_x_tiponodo_c")
	public Long getTipoNodo_c() {
		return tipoNodo_c;
	}

	@JsonProperty("d_parteremitida")
	public String getParteRemitida() {
		return parteRemitida;
	}

	@JsonProperty("edo_x_estadodocumento")
	public Long getEstadoDocumento() {
		return estadoDocumento;
	}

	@JsonProperty("doc_x_documento")
	public Long getDocXDocumento() {
		return docXDocumento;
	}

	@JsonProperty("tno_x_tiponodo_d")
	public Long getTipoNodo_d() {
		return tipoNodo_d;
	}

	@JsonProperty("d_terceroseintervinientes")
	public String getTercerosEIntervinientes() {
		return tercerosEIntervinientes;
	}

	@JsonProperty("cua_x_cuadroremisor")
	public Long getCuadroRemisor() {
		return cuadroRemisor;
	}

	@JsonProperty("cua_x_cuadroserie")
	public Long getCuadroSerie() {
		return cuadroSerie;
	}

	@JsonProperty("x_ingreso")
	public Long getIngreso() {
		return ingreso;
	}

	@JsonProperty("d_productor_i")
	public String getProductor_i() {
		return productor_i;
	}

	@JsonProperty("cua_x_productor")
	public Long getCuadroProductor() {
		return cuadroProductor;
	}

	@JsonProperty("c_ingresoano")
	public Long getIngresoAno() {
		return ingresoAno;
	}

	@JsonProperty("c_ingreso")
	public Long getCodigoIngreso() {
		return codigoIngreso;
	}

	@JsonProperty("tin_x_tiposingreso")
	public Long getTipoIngreso() {
		return tipoIngreso;
	}

	@JsonProperty("ein_x_estadosingreso")
	public Long getEstadoIngreso() {
		return estadoIngreso;
	}

	@JsonProperty("tno_x_tiponodo_i")
	public Long getTipoNodo_i() {
		return tipoNodo_i;
	}

	@JsonProperty("f_fecha_extrema_inicial")
	public Date getFechaExtremaInicial() {
		return fechaExtremaInicial;
	}

	@JsonProperty("f_fecha_extrema_final")
	public Date getFechaExtremaFinal() {
		return fechaExtremaFinal;
	}

	@JsonProperty("x_isadg")
	public Long getIsadg() {
		return isadg;
	}

	@JsonProperty("d_productor_is")
	public String getProductor_is() {
		return productor_is;
	}

	@JsonProperty("t_alcance")
	public String getAlcance() {
		return alcance;
	}

	@JsonProperty("t_histins")
	public String getHistins() {
		return histins;
	}

	@JsonProperty("t_valoracion")
	public String getValoracion() {
		return valoracion;
	}

	@JsonProperty("d_acceso")
	public String getAcceso() {
		return acceso;
	}

	@JsonProperty("d_cfisicas")
	public String getcFisicas() {
		return cFisicas;
	}

	@JsonProperty("t_npublicaciones")
	public String getnPublicaciones() {
		return nPublicaciones;
	}

	@JsonProperty("t_notas")
	public String getNotas() {
		return notas;
	}

	@JsonProperty("c_diainicial")
	public Long getDiaInicial() {
		return diaInicial;
	}

	@JsonProperty("c_mesinicial")
	public Long getMesInicial() {
		return mesInicial;
	}

	@JsonProperty("c_anoinicial")
	public Long getAnoInicial() {
		return anoInicial;
	}

	@JsonProperty("c_diafinal")
	public Long getDiaFinal() {
		return diaFinal;
	}

	@JsonProperty("c_mesfinal")
	public Long getMesFinal() {
		return mesFinal;
	}

	@JsonProperty("c_anofinal")
	public Long getAnoFInal() {
		return anoFInal;
	}

	@JsonProperty("d_titulo")
	public String getTitulo() {
		return titulo;
	}

	@JsonProperty("x_cuadro")
	public Long getCuadro() {
		return cuadro;
	}

	@JsonProperty("d_cuadro")
	public String getdCuadro() {
		return dCuadro;
	}

	@JsonProperty("cua_x_cuadro")
	public Long getCuaXCuadro() {
		return cuaXCuadro;
	}

	@JsonProperty("isa_x_isadg_c")
	public Long getIsaXIsadgC() {
		return isaXIsadgC;
	}

	@JsonProperty("rse_x_serie")
	public Long getRseXSerie() {
		return rseXSerie;
	}

	@JsonProperty("x_signatura")
	public Long getSignatura() {
		return signatura;
	}

	@JsonProperty("c_signatura")
	public Long getcSignatura() {
		return cSignatura;
	}

	@JsonProperty("d_etiqueta")
	public String getEtiqueta() {
		return etiqueta;
	}

	@JsonProperty("d_tiponodo")
	public String getTipoNodo() {
		return tipoNodo;
	}

	@JsonProperty("l_serie")
	public String getlSerie() {
		return lSerie;
	}

	@JsonProperty("l_publicado")
	public String getlPublicado() {
		return lPublicado;
	}

	@JsonProperty("l_enviadoweb")
	public String getlEnviadoWeb() {
		return lEnviadoWeb;
	}

	@JsonProperty("tno_x_tiponodo_atn")
	public Long getTnoXTipoNodo_atn() {
		return tnoXTipoNodo_atn;
	}

	@JsonProperty("f_documento")
	public String getfDocumento() {
		return fDocumento;
	}

	@JsonProperty("c_referenciaserie")
	public String getReferenciaSerie() {
		return referenciaSerie;
	}

	@JsonProperty("d_signaturacomplementaria")
	public String getSignaturaComplementaria() {
		return signaturaComplementaria;
	}

	@JsonProperty("c_salida")
	public Long getcSalida() {
		return cSalida;
	}

	@JsonProperty("c_salidaano")
	public Long getcSalidaAnio() {
		return cSalidaAnio;
	}

	@JsonProperty("pro_x_productor")
	public Long getProXProductor() {
		return proXProductor;
	}

	public void setArchivo(Long archivo) {
		this.archivo = archivo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public void setxDocumento(Long xDocumento) {
		this.xDocumento = xDocumento;
	}

	public void setRemisor(String remisor) {
		this.remisor = remisor;
	}

	public void setRemisor_i(String remisor_i) {
		this.remisor_i = remisor_i;
	}

	public void setClaveExp(String claveExp) {
		this.claveExp = claveExp;
	}

	public void setTituloExp(String tituloExp) {
		this.tituloExp = tituloExp;
	}

	public void setClaveTramitacion(String claveTramitacion) {
		this.claveTramitacion = claveTramitacion;
	}

	public void setTituloTramitacion(String tituloTramitacion) {
		this.tituloTramitacion = tituloTramitacion;
	}

	public void setNig(String nig) {
		this.nig = nig;
	}

	public void setNaturaleza(String naturaleza) {
		this.naturaleza = naturaleza;
	}

	public void setPrescadu(Date prescadu) {
		this.prescadu = prescadu;
	}

	public void setTerparejecu(Date terparejecu) {
		this.terparejecu = terparejecu;
	}

	public void setResprescadu(Date resprescadu) {
		this.resprescadu = resprescadu;
	}

	public void setObjeto(String objeto) {
		this.objeto = objeto;
	}

	public void setEstadoJudicial(String estadoJudicial) {
		this.estadoJudicial = estadoJudicial;
	}

	public void setProcedimientoRel(String procedimientoRel) {
		this.procedimientoRel = procedimientoRel;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

	public void setEstadoDocumento_h(Long estadoDocumento_h) {
		this.estadoDocumento_h = estadoDocumento_h;
	}

	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}

	public void setDescripcion_i(String descripcion_i) {
		this.descripcion_i = descripcion_i;
	}

	public void setVolumen(String volumen) {
		this.volumen = volumen;
	}

	public void setHistArchivo(String histArchivo) {
		this.histArchivo = histArchivo;
	}

	public void setIngreso_i(String ingreso_i) {
		this.ingreso_i = ingreso_i;
	}

	public void setNuevosIng(String nuevosIng) {
		this.nuevosIng = nuevosIng;
	}

	public void setiDescripcion(String iDescripcion) {
		this.iDescripcion = iDescripcion;
	}

	public void setOriginales(String originales) {
		this.originales = originales;
	}

	public void setCopias(String copias) {
		this.copias = copias;
	}

	public void setUdescrela(String udescrela) {
		this.udescrela = udescrela;
	}

	public void setnArchivero(String nArchivero) {
		this.nArchivero = nArchivero;
	}

	public void setNormas(String normas) {
		this.normas = normas;
	}

	public void setOrganizacion(String organizacion) {
		this.organizacion = organizacion;
	}

	public void setReproduccion(String reproduccion) {
		this.reproduccion = reproduccion;
	}

	public void setLengua(String lengua) {
		this.lengua = lengua;
	}

	public void setTipoNodo_c(Long tipoNodo_c) {
		this.tipoNodo_c = tipoNodo_c;
	}

	public void setParteRemitida(String parteRemitida) {
		this.parteRemitida = parteRemitida;
	}

	public void setEstadoDocumento(Long estadoDocumento) {
		this.estadoDocumento = estadoDocumento;
	}

	public void setDocXDocumento(Long docXDocumento) {
		this.docXDocumento = docXDocumento;
	}

	public void setTipoNodo_d(Long tipoNodo_d) {
		this.tipoNodo_d = tipoNodo_d;
	}

	public void setTercerosEIntervinientes(String tercerosEIntervinientes) {
		this.tercerosEIntervinientes = tercerosEIntervinientes;
	}

	public void setCuadroRemisor(Long cuadroRemisor) {
		this.cuadroRemisor = cuadroRemisor;
	}

	public void setCuadroSerie(Long cuadroSerie) {
		this.cuadroSerie = cuadroSerie;
	}

	public void setIngreso(Long ingreso) {
		this.ingreso = ingreso;
	}

	public void setProductor_i(String productor_i) {
		this.productor_i = productor_i;
	}

	public void setCuadroProductor(Long cuadroProductor) {
		this.cuadroProductor = cuadroProductor;
	}

	public void setIngresoAno(Long ingresoAno) {
		this.ingresoAno = ingresoAno;
	}

	public void setCodigoIngreso(Long codigoIngreso) {
		this.codigoIngreso = codigoIngreso;
	}

	public void setTipoIngreso(Long tipoIngreso) {
		this.tipoIngreso = tipoIngreso;
	}

	public void setEstadoIngreso(Long estadoIngreso) {
		this.estadoIngreso = estadoIngreso;
	}

	public void setTipoNodo_i(Long tipoNodo_i) {
		this.tipoNodo_i = tipoNodo_i;
	}

	public void setFechaExtremaInicial(Date fechaExtremaInicial) {
		this.fechaExtremaInicial = fechaExtremaInicial;
	}

	public void setFechaExtremaFinal(Date fechaExtremaFinal) {
		this.fechaExtremaFinal = fechaExtremaFinal;
	}

	public void setIsadg(Long isadg) {
		this.isadg = isadg;
	}

	public void setProductor_is(String productor_is) {
		this.productor_is = productor_is;
	}

	public void setAlcance(String alcance) {
		this.alcance = alcance;
	}

	public void setHistins(String histins) {
		this.histins = histins;
	}

	public void setValoracion(String valoracion) {
		this.valoracion = valoracion;
	}

	public void setAcceso(String acceso) {
		this.acceso = acceso;
	}

	public void setcFisicas(String cFisicas) {
		this.cFisicas = cFisicas;
	}

	public void setnPublicaciones(String nPublicaciones) {
		this.nPublicaciones = nPublicaciones;
	}

	public void setNotas(String notas) {
		this.notas = notas;
	}

	public void setDiaInicial(Long diaInicial) {
		this.diaInicial = diaInicial;
	}

	public void setMesInicial(Long mesInicial) {
		this.mesInicial = mesInicial;
	}

	public void setAnoInicial(Long anoInicial) {
		this.anoInicial = anoInicial;
	}

	public void setDiaFinal(Long diaFinal) {
		this.diaFinal = diaFinal;
	}

	public void setMesFinal(Long mesFinal) {
		this.mesFinal = mesFinal;
	}

	public void setAnoFInal(Long anoFInal) {
		this.anoFInal = anoFInal;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public void setCuadro(Long cuadro) {
		this.cuadro = cuadro;
	}

	public void setdCuadro(String dCuadro) {
		this.dCuadro = dCuadro;
	}

	public void setCuaXCuadro(Long cuaXCuadro) {
		this.cuaXCuadro = cuaXCuadro;
	}

	public void setIsaXIsadgC(Long isaXIsadgC) {
		this.isaXIsadgC = isaXIsadgC;
	}

	public void setRseXSerie(Long rseXSerie) {
		this.rseXSerie = rseXSerie;
	}

	public void setSignatura(Long signatura) {
		this.signatura = signatura;
	}

	public void setcSignatura(Long cSignatura) {
		this.cSignatura = cSignatura;
	}

	public void setEtiqueta(String etiqueta) {
		this.etiqueta = etiqueta;
	}

	public void setTipoNodo(String tipoNodo) {
		this.tipoNodo = tipoNodo;
	}

	public void setlSerie(String lSerie) {
		this.lSerie = lSerie;
	}

	public void setlPublicado(String lPublicado) {
		this.lPublicado = lPublicado;
	}

	public void setlEnviadoWeb(String lEnviadoWeb) {
		this.lEnviadoWeb = lEnviadoWeb;
	}

	public void setTnoXTipoNodo_atn(Long tnoXTipoNodo_atn) {
		this.tnoXTipoNodo_atn = tnoXTipoNodo_atn;
	}

	public void setfDocumento(String fDocumento) {
		this.fDocumento = fDocumento;
	}

	public void setReferenciaSerie(String referenciaSerie) {
		this.referenciaSerie = referenciaSerie;
	}

	public void setSignaturaComplementaria(String signaturaComplementaria) {
		this.signaturaComplementaria = signaturaComplementaria;
	}

	public void setcSalida(Long cSalida) {
		this.cSalida = cSalida;
	}

	public void setcSalidaAnio(Long cSalidaAnio) {
		this.cSalidaAnio = cSalidaAnio;
	}

	public void setProXProductor(Long proXProductor) {
		this.proXProductor = proXProductor;
	}
}
