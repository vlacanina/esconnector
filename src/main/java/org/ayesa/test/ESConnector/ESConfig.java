package org.ayesa.test.ESConnector;

import java.net.InetAddress;
import java.net.UnknownHostException;

import org.apache.catalina.core.ApplicationContext;
import org.elasticsearch.client.Client;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.InetSocketTransportAddress;
import org.elasticsearch.transport.client.PreBuiltTransportClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.elasticsearch.core.ElasticsearchOperations;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.data.elasticsearch.repository.config.EnableElasticsearchRepositories;

/** Configuración del cliente para comunicación con ES
 * 
 * @author vlacanina
 *
 */
@Configuration
@EnableElasticsearchRepositories
public class ESConfig {
	
	@Value("${elastic.nodeName}")
	private String nodeName;
	
	@Value("${elastic.clusterName}")
	private String clusterName;
	
	@Value("${elastic.host}")
	private String host;
	
	@Value("${elastic.port}")
	private int port;
	
	@Value("${elastic.indexDocument}")
	private String indexDocument;
	
	@Value("${elastic.indexType}")
	private String indexType;
	

	@Bean
	public Client client() {
		Settings elasticSettings = Settings.builder().put("cluster.name", clusterName).put("node.name", nodeName).build();
		TransportClient client = null;
		try {
			client = new PreBuiltTransportClient(elasticSettings);
			client.addTransportAddress(new InetSocketTransportAddress(InetAddress.getByName(host), port));
			
		}catch(UnknownHostException e) {
			e.printStackTrace();
			client.close();
		}
		return client;
	}
	
	@Bean
	public ElasticsearchOperations elasticsearchTemplate() {
		return new ElasticsearchTemplate(client());
	}
	
	@Bean
	public String nodeName() {
		return nodeName;
	}
	@Bean
	public String clusterName() {
		return clusterName;
	}
	@Bean
	public String host() {
		return host;
	}
	@Bean
	public int port() {
		return port;
	}
	@Bean
	public String indexDocument() {
		return indexDocument;
	}
	@Bean
	public String indexType() {
		return indexType;
	}

}
