package org.ayesa.test.ESConnector;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ESConnectorApplication {

	public static void main(String[] args) {
		SpringApplication.run(ESConnectorApplication.class, args);
	}

}
