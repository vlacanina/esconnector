package org.ayesa.test.ESConnector.repositories;

import java.util.List;

import org.ayesa.test.ESConnector.model.DocumentoDAO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.annotations.Query;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DocumentoRepository extends ElasticsearchRepository<DocumentoDAO, String> {
	
	@Query("{\"match_all\": {}}")
	List<DocumentoDAO> findAllDocuments();

}
