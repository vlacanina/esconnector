package org.ayesa.test.ESConnector.services;

import java.util.List;

import org.ayesa.test.ESConnector.model.DocumentoDAO;

public interface SearchDocument {

	List<DocumentoDAO> listAllDocuments();
}
