package org.ayesa.test.ESConnector.services;

import java.util.List;

import org.ayesa.test.ESConnector.model.DocumentoDAO;
import org.ayesa.test.ESConnector.repositories.DocumentoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SearchDocumentImpl implements SearchDocument{
	
	private final DocumentoRepository documentRepository;
	
	@Autowired
	public SearchDocumentImpl(DocumentoRepository documentoRepository) {
		this.documentRepository = documentoRepository;
	}

	@Override
	public List<DocumentoDAO> listAllDocuments() {
		return documentRepository.findAllDocuments();

	}

}
