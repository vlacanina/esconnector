package org.ayesa.test.ESConnector.controller;

import java.util.List;

import org.ayesa.test.ESConnector.model.DocumentoDAO;
import org.ayesa.test.ESConnector.services.SearchDocument;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RequestDocuments {
	
	private final SearchDocument documentService;
	
	@Autowired
	public RequestDocuments(SearchDocument documentService) {
		this.documentService = documentService;
	}

	
	@RequestMapping("/documents")
	public Object [] listDocuments(final Model model) {
		List<DocumentoDAO> list = documentService.listAllDocuments();
		return list.toArray();
	}
}
